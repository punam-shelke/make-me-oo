package org.oop;

public class Point {
    private final double x;
    private final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double distance(Point point) {
        if (point == null) return -1;
        double xDistance = getDistance(point.x, x);
        double yDistance = getDistance(point.y, y);
        return Math.sqrt(square(xDistance) + square(yDistance));
    }

    public double direction(Point point) {
        if (point == null) return 2 * Math.PI;
        double xDistance = getDistance(point.x, x);
        double yDistance = getDistance(point.y, y);
        return Math.atan2(yDistance, xDistance);
    }

    private double getDistance(double distance1, double distance2) {
        return distance1 - distance2;
    }

    private double square(double distance) {
        return Math.pow(distance, 2);
    }
}
